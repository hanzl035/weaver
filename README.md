# Weaver

A library for stitching together text files. It was designed to be used as
part of a Server Side Include inspired Static Site Generator, therefore it
handles html, js, and css files by default. By editing the mime and 
reviver variables at the top of weaver.mjs it is possible to implement
additional file formats. By default Weaver will assume html style comments.

All # prepended block comments function like a SSI include command.
Insert ${variable} into the fileurl to customize the fileurls at runtime. 
All % prepended block comments function like a SSI var command.

# Required Modules

Weaver uses commonmark to function. Running 'npm install commonmark'
in the directory with weaver.mjs will solve any module dependency issues.
