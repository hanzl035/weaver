import fs from "fs"
import path from "path"
import * as commonmark from "commonmark"

/*----------------------------------------------------------------------------
// command overview
const weaver = new Weaver([[key, filepart]], [[key, insert]])
const text = weaver.fetch(fileurl)
// editing after the fact
weaver.woptoins.set(key, filepart)
weaver.soptions.set(key, insert)
----------------------------------------------------------------------------*/

const mime = new Map([
 [".css", ["\/\\*", "\\*\/"]],
 [".js", ["\/\\*", "\\*\/"]],
 [".mjs", ["\/\\*", "\\*\/"]]
])
const comments = ["<!--", "-->"]

const reader = new commonmark.Parser()
const writer = new commonmark.HtmlRenderer()

const revivers = new Map([
 [
  ".md",
  (text) => {
   const parsed = reader.parse(text)
   return writer.render(parsed).trim() // remove trim() for whitespace
  }
 ]
])

class Weaver {
 #soptions
 #eoptions
 #woptions
 constructor(woptions = [], soptions = [], _eoptions = revivers) {
  this.#woptions = new Map(woptions)
  this.#soptions = new Map(soptions)
  this.#eoptions = new Map(_eoptions)
 }
 get soptions() {
  return this.#soptions
 }
 get eoptions() {
  return this.#eoptions
 }
 get woptions() {
  return this.#woptions
 }

 _replace(fileurl) {
  const global_exp = new RegExp("\\${[^]+?}", "g")
  const local_exp = new RegExp("(?<=\\${)[^]+?(?=})")
  const func = (match) => {
   const key = local_exp.exec(match)[0]
   if (!this.woptions.has(key)) {
    const message = `found ${match} with no specified weaver option`
    throw new Error(message)
   }
   return this.woptions.get(key)
  }
  return fileurl.replace(global_exp, func)
 }

 _shred(text, lcomment, rcomment) {
  const global_exp = new RegExp(lcomment + "[^]+?" + rcomment, "g")
  const lookbehind = `(?<=${ lcomment })`
  const lookahead = `(?=${ rcomment })`
  const local_exp = new RegExp(lookbehind + "[^]+?" + lookahead)
  const lines = text.split(global_exp)
  const oddlines = [...text.matchAll(global_exp)].map(comment => {
   const innards = local_exp.exec(comment)[0]
   return this._replace(innards)
  })
  return [lines, oddlines]
 }

 _splice(text, ext) {
  let lcomment, rcomment
  if (mime.has(ext)) [lcomment, rcomment] = mime.get(ext)
  else [lcomment, rcomment] = comments
  lcomment = lcomment + "%"
  const global_exp = new RegExp(lcomment + "[^]+?" + rcomment, "g")
  const lookbehind = `(?<=${ lcomment })`
  const lookahead = `(?=${ rcomment })`
  const local_exp = new RegExp(lookbehind + "[^]+?" + lookahead)
  const lines = text.split(global_exp)
  const oddlines = [...text.matchAll(global_exp)].map(comment => {
   const key = local_exp.exec(comment)[0]
   if (!this.soptions.has(key)) {
    const message = `${key} not in splice options`
    throw new Error(message)
   }
   else return this.soptions.get(key)
  })
  return lines.reduce((acc, cur, i) => {
   return acc + oddlines[i-1] + cur
  })
 }

 _expand(text, ext) {
  if (!this.eoptions.has(ext)) {
   return text
  }
  else return this.eoptions.get(ext)(text)
 }

 async _pull(fileurl) {
  let text = await fs.promises.readFile(fileurl).then(result => {
   return String(result).trim() // remove trim() to preserve whitespace
  })
  const ext = path.extname(fileurl)
  text = this._splice(text, ext)
  text = this._expand(text, ext)
  let lcomment, rcomment
  if (mime.has(ext)) [lcomment, rcomment] = mime.get(ext)
  else [lcomment, rcomment] = comments
  lcomment = lcomment + "#"
  return [text, lcomment, rcomment]
 }

 async _weave(lines, fileurls, _cache = new Map()) {
  const odd_lines = await Promise.all(
   fileurls.map(async fileurl => {
    let template = _cache.get(fileurl)
    if (typeof template == "undefined") {
     template = await this._pull(fileurl)
     _cache.set(fileurl, template)
    }
    const text = this._weave(...this._shred(...template), _cache)
    return text
   })
  )
  return lines.reduce((acc, cur, i) => {
   return acc + odd_lines[i-1] + cur
  })
 }

 async fetch(fileurl) {
  const template = await this._pull(fileurl)
  return this._weave(...this._shred(...template))
 }
}

export default Weaver
